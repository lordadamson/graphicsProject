#include "ApplicationManager/ApplicationManager.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

Mix_Music *MUSIC_HANDLER = NULL;

int main(void)
{
     Mix_Music* music;
     Mix_OpenAudio(22050 , MIX_DEFAULT_FORMAT, 2, 4096);
     music=Mix_LoadMUS("8-bit-music.wav");
       Mix_PlayMusic(music, -1);

	//Specify OpenGL version as 3.3
	int openGLMajorVersion = 3;
	int openGLMinorVersion = 3;

	//Unique object for the application manager
	std::unique_ptr<ApplicationManager> appManager (new ApplicationManager(openGLMajorVersion,openGLMinorVersion));
	
	// Initialize the window providing its width and height
    if(appManager->InitalizeApplication(2600,1500) == true)
	{
		appManager->StartMainLoop();
		appManager->CloseApplication();
	}

    Mix_FreeMusic(music);
    Mix_CloseAudio();

}
