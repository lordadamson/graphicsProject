﻿#include "Renderer.h"

#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include "OBJLoader/objloader.hpp"

Renderer::Renderer()
{
    
}

Renderer::~Renderer()
{
    Cleanup();
}

void Renderer::Initialize()
{
    keyreleased = false;
    keypressed = false;
    right = false;
    space = false ;
    ctrl = false ;

	//myCamera = std::unique_ptr<FPCamera>(new FPCamera());
	myCamera = std::unique_ptr<EulerCamera>(new EulerCamera());
	collisionManager = std::unique_ptr<CollisionManager>( new CollisionManager());

	//////////////////////////////////////////////////////////////////////////
    //drawing a square.
    rightside = std::unique_ptr<Model>(new Model());

    rightside->VertexData.push_back(glm::vec3(1.0f, -1.0f, 0.0f));
    rightside->UVData.push_back(glm::vec2(0.0f,0.0f));
    rightside->VertexData.push_back(glm::vec3(1.0f,  -1.0f, 2.0f));
    rightside->UVData.push_back(glm::vec2(1.0f,0.0f));
    rightside->VertexData.push_back(glm::vec3( 1.0f, 1.0f, 0.0f));
    rightside->UVData.push_back(glm::vec2(1.0f,1.0f));
    rightside->VertexData.push_back(glm::vec3( 1.0f, 1.0f, 2.0f));
    rightside->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    rightside->IndicesData.push_back(0);
    rightside->IndicesData.push_back(1);
    rightside->IndicesData.push_back(3);

    //second triangle.
    rightside->IndicesData.push_back(0);
    rightside->IndicesData.push_back(2);
    rightside->IndicesData.push_back(3);
    glm::vec3 squareNormalright = glm::vec3(0.0,0.0,1.0);
    rightside->NormalsData.push_back(squareNormalright);
    rightside->NormalsData.push_back(squareNormalright);
    rightside->NormalsData.push_back(squareNormalright);
    rightside->NormalsData.push_back(squareNormalright);
    rightside->Initialize();

    rightsideTexture = std::unique_ptr<Texture>(new Texture("data/textures/1024x1024.jpg",0));

    //////////////////////////////////////////////////////////////////////////
    //drawing a square.
    leftside = std::unique_ptr<Model>(new Model());

    leftside->VertexData.push_back(glm::vec3(-1.0f, -1.0f, 2.0f));
    leftside->UVData.push_back(glm::vec2(0.0f,0.0f));
    leftside->VertexData.push_back(glm::vec3(1.0f,  -1.0f, 2.0f));
    leftside->UVData.push_back(glm::vec2(1.0f,0.0f));
    leftside->VertexData.push_back(glm::vec3( -1.0f, -1.0f, 0.0f));
    leftside->UVData.push_back(glm::vec2(1.0f,1.0f));
    leftside->VertexData.push_back(glm::vec3( 1.0f, -1.0f, 0.0f));
    leftside->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    leftside->IndicesData.push_back(0);
    leftside->IndicesData.push_back(1);
    leftside->IndicesData.push_back(3);

    //second triangle.
    leftside->IndicesData.push_back(0);
    leftside->IndicesData.push_back(2);
    leftside->IndicesData.push_back(3);

    glm::vec3 squareNormalleft = glm::vec3(0.0,0.0,1.0);
    leftside->NormalsData.push_back(squareNormalleft);
    leftside->NormalsData.push_back(squareNormalleft);
    leftside->NormalsData.push_back(squareNormalleft);
    leftside->NormalsData.push_back(squareNormalleft);
    leftside->Initialize();

    leftsideTexture = std::unique_ptr<Texture>(new Texture("data/textures/1024x1024.jpg",0));

    // ///////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //drawing a square.
    floor = std::unique_ptr<Model>(new Model());

    floor->VertexData.push_back(glm::vec3(-1.0f, -1.0f, 0.0f));
    floor->UVData.push_back(glm::vec2(0.0f,0.0f));
    floor->VertexData.push_back(glm::vec3(1.0f, -1.0f, 0.0f));
    floor->UVData.push_back(glm::vec2(1.0f,0.0f));
    floor->VertexData.push_back(glm::vec3( 1.0f,  1.0f, 0.0f));
    floor->UVData.push_back(glm::vec2(1.0f,1.0f));
    floor->VertexData.push_back(glm::vec3( -1.0f,  1.0f, 0.0f));
    floor->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    floor->IndicesData.push_back(0);
    floor->IndicesData.push_back(1);
    floor->IndicesData.push_back(3);

    //second triangle.
    floor->IndicesData.push_back(1);
    floor->IndicesData.push_back(2);
    floor->IndicesData.push_back(3);


    glm::vec3 squareNormal = glm::vec3(0.0,0.0,1.0);
    floor->NormalsData.push_back(squareNormal);
    floor->NormalsData.push_back(squareNormal);
    floor->NormalsData.push_back(squareNormal);
    floor->NormalsData.push_back(squareNormal);
    floor->Initialize();

    floorTexture = std::unique_ptr<Texture>(new Texture("data/textures/grass.jpg",0));


    //drawing a square.
    back = std::unique_ptr<Model>(new Model());

 back->VertexData.push_back(glm::vec3(-1.0f, 1.0f, 2.0f));
 back->UVData.push_back(glm::vec2(0.0f,0.0f));
 back->VertexData.push_back(glm::vec3(-1.0f,  -1.0f, 2.0f));
back ->UVData.push_back(glm::vec2(1.0f,0.0f));
 back->VertexData.push_back(glm::vec3( -1.0f, -1.0f, 0.0f));
 back ->UVData.push_back(glm::vec2(1.0f,1.0f));
 back ->VertexData.push_back(glm::vec3( -1.0f, 1.0f, 0.0f));
  back->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    back->IndicesData.push_back(0);
    back->IndicesData.push_back(1);
    back->IndicesData.push_back(3);

    //second triangle.
    back->IndicesData.push_back(1);
    back->IndicesData.push_back(2);
    back->IndicesData.push_back(3);

    glm::vec3 squareNormalback = glm::vec3(0.0,0.0,1.0);
    back->NormalsData.push_back(squareNormalback);
    back->NormalsData.push_back(squareNormalback);
    back->NormalsData.push_back(squareNormalback);
    back->NormalsData.push_back(squareNormalback);
    back->Initialize();

    backTexture = std::unique_ptr<Texture>(new Texture("data/textures/1024x1024.jpg",0));

    ///////////////////////////////////////////////////////////////////////////////
    //drawing a square.
    front = std::unique_ptr<Model>(new Model());

    front->VertexData.push_back(glm::vec3(1.0f, 1.0f, 2.0f));
    front->UVData.push_back(glm::vec2(0.0f,0.0f));
    front->VertexData.push_back(glm::vec3(-1.0f,  1.0f, 2.0f));
   front->UVData.push_back(glm::vec2(1.0f,0.0f));
    front->VertexData.push_back(glm::vec3( -1.0f, -1.0f, 2.0f));
   front ->UVData.push_back(glm::vec2(1.0f,1.0f));
   front ->VertexData.push_back(glm::vec3( 1.0f, -1.0f, 2.0f));
   front ->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    front->IndicesData.push_back(0);
    front->IndicesData.push_back(1);
    front->IndicesData.push_back(3);

    //second triangle.
    front->IndicesData.push_back(1);
    front->IndicesData.push_back(2);
    front->IndicesData.push_back(3);

    glm::vec3 squareNormalfront = glm::vec3(0.0,0.0,1.0);
    front->NormalsData.push_back(squareNormalfront);
    front->NormalsData.push_back(squareNormalfront);
    front->NormalsData.push_back(squareNormalfront);
    front->NormalsData.push_back(squareNormalfront);
    front->Initialize();

    frontTexture = std::unique_ptr<Texture>(new Texture("data/textures/1024x1024.jpg",0));



	//////////////////////////////////////////////////////////////////////////


    //drawing a square.
    up = std::unique_ptr<Model>(new Model());

    up->VertexData.push_back(glm::vec3(-1.0f, 1.0f, 0.0f));
    up->UVData.push_back(glm::vec2(0.0f,0.0f));
    up->VertexData.push_back(glm::vec3(1.0f,  1.0f, 0.0f));
   up->UVData.push_back(glm::vec2(1.0f,0.0f));
    up->VertexData.push_back(glm::vec3( 1.0f, 1.0f, 2.0f));
   up ->UVData.push_back(glm::vec2(1.0f,1.0f));
   up ->VertexData.push_back(glm::vec3( -1.0f, 1.0f, 2.0f));
   up ->UVData.push_back(glm::vec2(0.0f,1.0f));

    //first triangle.
    up->IndicesData.push_back(0);
    up->IndicesData.push_back(1);
    up->IndicesData.push_back(3);

    //second triangle.
    up->IndicesData.push_back(1);
    up->IndicesData.push_back(2);
    up->IndicesData.push_back(3);

    glm::vec3 squareNormalup = glm::vec3(0.0,0.0,1.0);
    up->NormalsData.push_back(squareNormalup);
    up->NormalsData.push_back(squareNormalup);
    up->NormalsData.push_back(squareNormalup);
    up->NormalsData.push_back(squareNormalup);
    up->Initialize();


    upTexture = std::unique_ptr<Texture>(new Texture("data/textures/1024x1024.jpg",0));
    ////////////////////////////////////////////////////////////////////////////////////////
	blade.LoadModel("data/models/blade/Blade.md2");
	//blade.LoadModel("data/models/samourai/Samourai.md2");
	bladeAnimationState = blade.StartAnimation(animType_t::STAND);
	//calculate AABB
	blade.SetBoundingBox(CollidableModel::CalculateBoundingBox(blade.GetVertices()));
	collisionManager->AddCollidableModel((CollidableModel*) &blade);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//load model.
	mySpider = std::unique_ptr<Model3D>(new Model3D());
	//read model and it's textures from HDD.
	mySpider->LoadFromFile("data/models/Spider/spider.obj",true);
	//send the meshes to the GPU.
	mySpider->Initialize();
	mySpider->SetBoundingBox(CollidableModel::CalculateBoundingBox(mySpider->GetVertices()));
	collisionManager->AddCollidableModel((CollidableModel*) mySpider.get());
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	house = std::unique_ptr<Model3D>(new Model3D());
	house->LoadFromFile("data/models/House/house.3ds");
	house->Initialize();
	house->SetBoundingBox(CollidableModel::CalculateBoundingBox(house->GetVertices(),-2.0f));
	collisionManager->AddCollidableModel((CollidableModel*) house.get());
	//////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////
	// Create and compile our GLSL program from the shaders
	animatedModelShader.LoadProgram();
	staticModelShader.LoadProgram();
	//////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////
	// Projection matrix : 
	myCamera->SetPerspectiveProjection(45.0f,4.0f/3.0f,0.1f,100.0f);

	// View matrix : 
    myCamera->Reset(0.0,1.0,2.0,
                    0,0,0,
					0,1,0);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Configure the light.
	//setup the light position.
	staticModelShader.UseProgram();
	LightPositionID = glGetUniformLocation(staticModelShader.programID,"LightPosition_worldspace");
    lightPosition = glm::vec3(10000.0,10000.0,0.0);
	glUniform3fv(LightPositionID,1, &lightPosition[0]);
	//setup the ambient light component.
	AmbientLightID = glGetUniformLocation(staticModelShader.programID,"ambientLight");
	ambientLight = glm::vec3(0.1,0.1,0.1);
	glUniform3fv(AmbientLightID,1, &ambientLight[0]);
	//setup the eye position.
	EyePositionID = glGetUniformLocation(staticModelShader.programID,"EyePosition_worldspace");
	//send the eye position to the shaders.
    glUniform3fv(EyePositionID,1, &myCamera->GetEyePosition()[0]);

	///////////////////////////////////////////////////
	//repeat the process for the animated models shader.
	animatedModelShader.UseProgram();
	LightPositionID = glGetUniformLocation(animatedModelShader.programID,"LightPosition_worldspace");
	lightPosition = glm::vec3(1.0,0.25,0.0);
	glUniform3fv(LightPositionID,1, &lightPosition[0]);
	//setup the ambient light component.
	AmbientLightID = glGetUniformLocation(animatedModelShader.programID,"ambientLight");
	ambientLight = glm::vec3(0.5,0.5,0.5);
	glUniform3fv(AmbientLightID,1, &ambientLight[0]);
	//setup the eye position.
	EyePositionID = glGetUniformLocation(animatedModelShader.programID,"EyePosition_worldspace");
	//send the eye position to the shaders.
	glUniform3fv(EyePositionID,1, &myCamera->GetEyePosition()[0]);
	//////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////
	//Setting the initaial transformations
    floorM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));
    rightsideM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));
    leftsideM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));
    frontM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));
    backM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));
    upM =  glm::scale(30.0f,30.0f,30.0f)*glm::rotate(-90.0f,glm::vec3(1.0f,0.0f,0.0f));


    spiderM = glm::translate(-1.0f,0.25f,0.0f) * glm::scale(0.005f,0.005f,0.005f);
	//we have to scale and translate the spider's bounding box to match the new position and size.
	auto tempBox = mySpider->GetBoundingBox();
	tempBox.Scale(0.005f,0.005f,0.005f);
	tempBox.Translate(-1.0f,0.25f,0.0f);
	mySpider->SetBoundingBox(tempBox);
	
    bladeM = glm::rotate(-90.0f,1.0f,0.0f,0.0f) * glm::scale(0.01f,0.01f,0.01f) * glm::translate(0.0f, 0.0f, 24.0f);
    tempBox = blade.GetBoundingBox();
	tempBox.Scale(0.01f,0.01f,0.01f);
	tempBox.Rotate(-90.0f,1.0f,0.0f,0.0f);
	blade.SetBoundingBox(tempBox);

    houseM = glm::translate(10.0f, 0.0f, 0.0f) *glm::rotate(90.0f,1.0f,0.0f,0.0f) *glm::scale(0.1f,0.1f,0.1f);
	tempBox = house->GetBoundingBox();
	tempBox.Scale(0.1f,0.1f,0.1f);
	tempBox.Rotate(90.0f,1.0f,0.0f,0.0f);
    tempBox.Translate(10.0f, 0.0f, 0.0f);
	house->SetBoundingBox(tempBox);


	//////////////////////////////////////////////////////////////////////////

}

void Renderer::Draw()
{		

		//Bind the VP matrix (Camera matrices) to the current shader.
		glm::mat4 VP = myCamera->GetProjectionMatrix() * myCamera->GetViewMatrix();


		staticModelShader.UseProgram();
		staticModelShader.BindVPMatrix(&VP[0][0]);
		staticModelShader.BindModelMatrix(&floorM[0][0]);
		floorTexture->Bind();
		floor->Draw();


        rightsideTexture->Bind();

        rightside->Draw();

        leftsideTexture->Bind();

        leftside->Draw();

        upTexture->Bind();

        up->Draw();

        frontTexture->Bind();

        front->Draw();

        backTexture->Bind();

        back->Draw();

        mySpider->Render(&staticModelShader,spiderM);
		house->Render(&staticModelShader,houseM);
		
        if (space == true )
           { bladeAnimationState = blade.StartAnimation(animType_t::JUMP);}
        else if (right == false && keypressed == false && keyreleased == true)
          {  bladeAnimationState = blade.StartAnimation(animType_t::STAND);}
        else if (right == true && keypressed == true)
           { bladeAnimationState = blade.StartAnimation(animType_t::RUN);}
        else if (ctrl == true)
            {bladeAnimationState = blade.StartAnimation(animType_t::ATTACK);}

		animatedModelShader.UseProgram();
		animatedModelShader.BindVPMatrix(&VP[0][0]);
		animatedModelShader.BindModelMatrix(&bladeM[0][0]);
		blade.RenderModel(&bladeAnimationState,&animatedModelShader);

        keyreleased = false;
        keypressed  = false;
        right       = false;
        space = false ;
        ctrl = false;
}

void Renderer::Cleanup()
{
}

void Renderer::Update(double deltaTime)
{
	blade.UpdateAnimation(&bladeAnimationState,deltaTime/1000);
	collisionManager->UpdateCollisions();
}

void Renderer::HandleKeyboardInput(int key, int m_action)
{
	float step = 0.1f;
	auto tempbox = blade.GetBoundingBox();

    if (blade.getCollide() == false)
    {
            if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_PRESS )
            {
                keyreleased = false;
                keypressed = true;
                myCamera->Walk(step);
                bladeM = glm::translate(0.0f,0.0f,-step)*bladeM;
                tempbox.Translate(0.0f,0.0f,-step);
                Mix_Chunk* effect1;

                effect1= Mix_LoadWAV("footsteps.wav");
                Mix_PlayChannel(-1, effect1, 0);
            }
            else if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_REPEAT)
            {
                keypressed = false;
                keyreleased = false;
                myCamera->Walk(step);
                bladeM = glm::translate(0.0f,0.0f,-step)*bladeM;
                tempbox.Translate(0.0f,0.0f,-step);
                Mix_Chunk* effect1;

                effect1= Mix_LoadWAV("footsteps.wav");
                Mix_PlayChannel(-1, effect1, 0);
            }
            else if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_RELEASE )
            {
                keyreleased = true;
                keypressed = false;
                myCamera->Walk(step);
                bladeM = glm::translate(0.0f,0.0f,-step)*bladeM;
                tempbox.Translate(0.0f,0.0f,-step);
                Mix_Chunk* effect1;

                effect1= Mix_LoadWAV("footsteps.wav");
                Mix_PlayChannel(-1, effect1, 0);
            }

            if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_PRESS )
            {
                keypressed = true;
                keyreleased = false;
                myCamera->Walk(-step);
                bladeM = glm::translate(0.0f,0.0f,step)*bladeM;
                tempbox.Translate(0.0f,0.0f,step);
            }
            else if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_REPEAT )
            {
                keypressed = false;
                keyreleased = false;
                myCamera->Walk(-step);
                bladeM = glm::translate(0.0f,0.0f,step)*bladeM;
                tempbox.Translate(0.0f,0.0f,step);
            }
            else if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_RELEASE )
            {
                keyreleased = true;
                keypressed = false;
            }

            if ( ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_PRESS ) )
            {
                keypressed = true;
                keyreleased = false;
                right = true;
                myCamera->Strafe(step);
                right = true;
                bladeM = glm::translate(step,0.0f,0.0f)*bladeM;
                tempbox.Translate(step,0.0f,0.0f);
            }

            else if ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_REPEAT )
            {
                keypressed = false;
                keyreleased = false;
                right = true;
                myCamera->Strafe(step);
                right = true;
                bladeM = glm::translate(step,0.0f,0.0f)*bladeM;
                tempbox.Translate(step,0.0f,0.0f);
            }
            else if ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_RELEASE )
            {
                keyreleased = true;
                keypressed = false;
                right = false;
                myCamera->Strafe(step);
                bladeM = glm::translate(step,0.0f,0.0f)*bladeM;
                tempbox.Translate(step,0.0f,0.0f);
            }

            if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_PRESS )
            {
                keypressed = true;
                keyreleased = false;
                myCamera->Strafe(-step);
                bladeM = glm::translate(-step,0.0f,0.0f)*bladeM;
                tempbox.Translate(-step,0.0f,0.0f);
            }
            else if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_REPEAT )
            {
                keypressed = false;
                keyreleased = false;
                myCamera->Strafe(-step);
                bladeM = glm::translate(-step,0.0f,0.0f)*bladeM;
                tempbox.Translate(-step,0.0f,0.0f);
            }
            else if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_RELEASE )
            {
                keyreleased = true;
                keypressed = false;
            }

            if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_PRESS )
            {
                ctrl = true;
            }
            else if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_REPEAT )
            {
                ctrl = true;
            }
            else if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_RELEASE )
            {
            }

            else if ( key == GLFW_KEY_SPACE  && m_action == GLFW_PRESS )
            {
                space = true ;
                keyreleased = true;
                //myCamera->Strafe(step);
                //right = true;
               // bladeM = glm::translate(step,0.0f,0.0f)*bladeM;
               // tempbox.Translate(step,0.0f,0.0f);
            }
    }
    else
    {
        if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_PRESS )
        {
            bladeM = glm::translate(0.0f,0.0f,step*2 )*bladeM;
            myCamera->Walk(-step*2);
            tempbox.Translate(0.0f,0.0f,step*2);
            blade.setCollide(false);
        }
        else if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_REPEAT)
        {
            bladeM = glm::translate(0.0f,0.0f,step*2 )*bladeM;
            myCamera->Walk(-step*2);
            tempbox.Translate(0.0f,0.0f,step*2);
            blade.setCollide(false);
        }
        else if ( (key == GLFW_KEY_UP || key == GLFW_KEY_W) && m_action == GLFW_RELEASE )
        {
            bladeM = glm::translate(0.0f,0.0f,step*2 )*bladeM;
            myCamera->Walk(-step*2);
            tempbox.Translate(0.0f,0.0f,step*2);
            blade.setCollide(false);
        }

        if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_PRESS )
        {
            bladeM = glm::translate(0.0f,0.0f,-step*2 )*bladeM;
            myCamera->Walk(step*2);
            tempbox.Translate(0.0f,0.0f,-step*2);
            blade.setCollide(false);
        }
        else if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_REPEAT )
        {
            bladeM = glm::translate(0.0f,0.0f,-step*2 )*bladeM;
            myCamera->Walk(step*2);
            tempbox.Translate(0.0f,0.0f,-step*2);
            blade.setCollide(false);
        }
        else if ( (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) && m_action == GLFW_RELEASE )
        {
            bladeM = glm::translate(0.0f,0.0f,-step*2 )*bladeM;
            myCamera->Walk(step*2);
            tempbox.Translate(0.0f,0.0f,-step*2);
            blade.setCollide(false);
        }

        if ( ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_PRESS ) )
        {

        }

        else if ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_REPEAT )
        {

        }
        else if ( (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) && m_action == GLFW_RELEASE )
        {

        }

        if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_PRESS )
        {
        }
        else if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_REPEAT )
        {
        }
        else if ( (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) && m_action == GLFW_RELEASE )
        {
        }

        if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_PRESS )
        {
        }
        else if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_REPEAT )
        {
        }
        else if ( (key == GLFW_KEY_LEFT_CONTROL || key == GLFW_KEY_F) && m_action == GLFW_RELEASE )
        {
        }

        else if ( key == GLFW_KEY_SPACE  && m_action == GLFW_PRESS )
        {
        }
    }

	//Set the transformed bounding box again.
	blade.SetBoundingBox(tempbox);

	//continue the remaining movements.
	myCamera->UpdateViewMatrix();

	//update the eye position uniform.
	staticModelShader.UseProgram();
	EyePositionID = glGetUniformLocation(staticModelShader.programID,"EyePosition_worldspace");
	glUniform3fv(EyePositionID,1, &myCamera->GetEyePosition()[0]);

	animatedModelShader.UseProgram();
	EyePositionID = glGetUniformLocation(animatedModelShader.programID,"EyePosition_worldspace");
	glUniform3fv(EyePositionID,1, &myCamera->GetEyePosition()[0]);
}

void Renderer::HandleMouse(double deltaX,double deltaY)
{	
    //myCamera->Yaw(deltaX);
    //myCamera->Pitch(deltaY);
    //myCamera->UpdateViewMatrix();
}
