#ifndef Renderer_h__
#define Renderer_h__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory>


#include "Shaders/shader.hpp"
#include "Model/Model.h"
#include "EulerCamera/EulerCamera.h"
#include "Texture/texture.h"
#include "GraphicsDevice/ShaderProgram.h"
#include "Scene/Model3D.h"
#include "GraphicsDevice/KeyFrameAnimationShader.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "MD2Model/md2model.h"
#include "CollisionDetection/CollisionManager.h"

class Renderer
{


    KeyFrameAnimationShader animatedModelShader;
	ShaderProgram staticModelShader;
	// Transformation
	GLuint AmbientLightID;
	GLuint LightPositionID;
	GLuint EyePositionID;

	CMD2Model blade;
	animState_t bladeAnimationState;
	std::unique_ptr<Model> floor;
	std::unique_ptr<Texture> floorTexture;
    std::unique_ptr<Model> rightside;
    std::unique_ptr<Texture> rightsideTexture;
    std::unique_ptr<Model> leftside;
    std::unique_ptr<Texture> leftsideTexture;
    std::unique_ptr<Model> front;
    std::unique_ptr<Texture> frontTexture;
    std::unique_ptr<Model> back;
    std::unique_ptr<Texture> backTexture;
    std::unique_ptr<Model> up;
    std::unique_ptr<Texture> upTexture;
	std::unique_ptr<Model3D> mySpider;

	std::unique_ptr<Model3D> house;
	glm::mat4 houseM;

	std::unique_ptr<EulerCamera> myCamera;

	glm::mat4 floorM;
    glm::mat4 rightsideM;
    glm::mat4 leftsideM;
    glm::mat4 frontM;
    glm::mat4 backM;
    glm::mat4 upM;
	glm::mat4 spiderM;
	glm::mat4 bladeM;
	glm::vec3 ambientLight;
	glm::vec3 lightPosition;

    bool right;
    bool keypressed;
    bool keyreleased;
    bool space;
    bool ctrl;

	std::unique_ptr<CollisionManager> collisionManager;

public:
    Renderer();
    ~Renderer();
    
    void Initialize();
    void Draw();
    void HandleKeyboardInput(int key, int m_action);
	void HandleMouse(double deltaX,double deltaY);
	void Update(double deltaTime);
    void Cleanup();

    void setCollide(bool);
    bool getCollide();
};

#endif // Renderer_h__

