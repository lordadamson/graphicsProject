# graphicsProject
runs on qtcreator

here is a list of package you'll need to install to get this up and running:</br>

open the terminal and type the following: </br>

on **ubuntu**: 
`sudo apt-get install libglu1-mesa-dev mesa-common-dev libgl1-mesa-dev libx11-dev build-essential libassimp-dev qtcreator glew-utils libglew-dev libglfw3-dev libsdl2-dev libsdl2-mixer-dev qt5-default` </br>
on **fedora**: 
`sudo yum install make automake gcc gcc-c++ kernel-devel mesa-libGLU-devel glew-devel derelict-GLFW3-devel assimp-devel libX11-devel qt-creator SDL2_mixer-devel SDL2-devel`</br>
